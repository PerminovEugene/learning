package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func pop(a []bool) []bool {
	_, a = a[len(a)-1], a[:len(a)-1]
	return a
}

func printOffset(nodeParentsAreLastInTree []bool, output io.Writer) {
	const parentNodeWithNextNodeOffset = "│\t"
	const lastParentNodeOffset = "\t"

	var i int
	for i < len(nodeParentsAreLastInTree)-1 {
		isLast := nodeParentsAreLastInTree[i]
		if isLast {
			fmt.Fprint(output, lastParentNodeOffset)
		} else {
			fmt.Fprint(output, parentNodeWithNextNodeOffset)
		}
		i++
	}
}

func printBranch(nodeParentsAreLastInTree []bool, output io.Writer) {
	const branchStart = "├"
	const lastBranchStart = "└"
	const branch = "───"
	if nodeParentsAreLastInTree[len(nodeParentsAreLastInTree)-1] {
		fmt.Fprint(output, lastBranchStart)
	} else {
		fmt.Fprint(output, branchStart)
	}
	fmt.Fprint(output, branch)
}

func printGraphics(nodeParentsAreLastInTree []bool, output io.Writer) {
	printOffset(nodeParentsAreLastInTree, output)
	printBranch(nodeParentsAreLastInTree, output)
}

func printFileDetails(file os.FileInfo, output io.Writer) {
	fmt.Fprint(output, " (")
	if file.Size() == 0 {
		fmt.Fprint(output, "empty")
	} else {
		fmt.Fprint(output, file.Size(), "b")
	}
	fmt.Fprint(output, ")")
}

func changeLastNodeParentsAreLastInTree(nodeParentsAreLastInTree []bool, newValue bool) {
	nodeParentsAreLastInTree[len(nodeParentsAreLastInTree)-1] = newValue
}

func filterFiles(files []os.FileInfo) []os.FileInfo {
	filtered := files[:0]
	for _, file := range files {
		if file.IsDir() {
			filtered = append(filtered, file)
		}
	}
	return filtered
}

func buildNewPath(path string, file os.FileInfo) string {
	var pathBuffer bytes.Buffer
	pathBuffer.WriteString(path)
	pathBuffer.WriteString("/")
	pathBuffer.WriteString(file.Name())
	return pathBuffer.String()
}

func handleNode(path string, output io.Writer, nodeParentsAreLastInTree []bool, printFiles bool) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		panic(err.Error())
	}
	if !printFiles {
		files = filterFiles(files)
	}

	idx := 0
	for idx < len(files) {
		var isLast bool = idx == len(files)-1
		file := files[idx]
		changeLastNodeParentsAreLastInTree(nodeParentsAreLastInTree, isLast)
		printGraphics(nodeParentsAreLastInTree, output)
		fmt.Fprint(output, file.Name())
		if !file.IsDir() {
			printFileDetails(file, output)
		}
		fmt.Fprint(output, "\n")

		if file.IsDir() {
			newPath := buildNewPath(path, file)
			nodeParentsAreLastInTree = append(nodeParentsAreLastInTree, isLast)
			handleNode(newPath, output, nodeParentsAreLastInTree, printFiles)
			nodeParentsAreLastInTree = pop(nodeParentsAreLastInTree)
		}
		idx++
	}
}

func dirTree(output io.Writer, path string, printFiles bool) error {
	var nodeParentsAreLastInTree []bool
	nodeParentsAreLastInTree = append(nodeParentsAreLastInTree, false)
	handleNode(path, output, nodeParentsAreLastInTree, printFiles)
	return nil
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
