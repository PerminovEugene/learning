import { ConcereteImplementation } from './concrete-implementation';
import { Implementation } from './implementation';
import { RefinedAbstaction } from './refined-abstaction';

export const start = () => {
  const concereteImplementation: Implementation = new ConcereteImplementation();
  const refinedAbstaction: RefinedAbstaction = new RefinedAbstaction(concereteImplementation);
  refinedAbstaction.doSomething();
};

start();
