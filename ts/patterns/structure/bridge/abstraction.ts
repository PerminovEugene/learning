import { Implementation } from './implementation';

export abstract class Abstraction {
  protected implementation: Implementation;

  constructor(implementation: Implementation) {
    this.implementation = implementation;
  }

  public doSomething = () => {
    console.log('doSomething');
  };
}
